# -*- coding: utf-8 -*-
import time
import tweepy
import csv

# Datos de la app RTRobotLoduCode
consumer_key = 'LxZBuOsST6wpKQ9k0Xw0fRtRY'
consumer_secret = 'sC7Oti2bJkjjziLTySG5DjnjHCSl3BL9GAqYqBq5bEZRKOjGAK'

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)

users = []
users_RT = []
change = False
change_users_RT = False

while(True):
    with open('usuarios.csv', newline='') as csvfile:
        spamreader = csv.DictReader(csvfile, delimiter=',', quotechar='|')
        for row in spamreader:
            users.append({
                'user': row['user'],
                'access_token': row['access_token'],
                'access_token_secret': row['access_token_secret']
            })

    for user in users:
        print("----------------------------------")
        print("For the user: " + user['user'])
        print("----------------------------------")
        access_token = user['access_token']
        access_token_secret = user['access_token_secret']
        if len(access_token) == 0 and len(access_token_secret) == 0:
            redirect_url = auth.get_authorization_url()
            try:
                print(
                    "Click in the  URL and auth the app, "
                    "copy the code.")
                print(redirect_url)
                verifier = input("Code twitter auth:")
                access_token, access_token_secret = auth.get_access_token(
                    verifier)
                user['access_token'] = access_token
                user['access_token_secret'] = access_token_secret
                change = True
                print("----------------------------------")
            except tweepy.TweepError:
                print('Error!')

        auth.set_access_token(access_token, access_token_secret)
        print("Success auth.")
        api = tweepy.API(auth)

        # Para hacer un Tweet
        # api.update_status('Prueba ')

        # Para hacer Retweet
        # api.retweet(1015267413691125761)

        with open('users_RT.csv', newline='') as csvfile:
            spamreader = csv.DictReader(csvfile, delimiter=',', quotechar='|')
            for row in spamreader:
                users_RT.append({
                    'user': row['user'],
                    'last_tweet': row['last_tweet'],
                })

        for RT in users_RT:
            tweets = api.user_timeline(screen_name=RT['user'],
                                       count=1,
                                       include_rts=False)
            for tweet in tweets:
                if RT['last_tweet'] != str(tweet.id):
                    print(tweet.id)
                    print(RT['last_tweet'])
                    RT['last_tweet'] = tweet.id
                    change_users_RT = True
                    api.retweet(tweet.id)
                    print("RT for: " + RT['user'])
                    break

    if change:
        with open('usuarios.csv', 'w', newline='') as csvfile:
            fieldnames = ['user', 'access_token', 'access_token_secret']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

            writer.writeheader()
            for user in users:
                writer.writerow({'user': user['user'],
                                 'access_token': user['access_token'],
                                 'access_token_secret': user[
                                     'access_token_secret']
                                 })
        change = False

    if change_users_RT:
        print("Entra a cambiar el ultimo tweet")
        with open('users_RT.csv', 'w', newline='') as csvfile:
            fieldnames = ['user', 'last_tweet']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

            writer.writeheader()
            for RT in users_RT:
                writer.writerow({'user': RT['user'],
                                 'last_tweet': RT['last_tweet']})

        change_users_RT = False

    users = []
    users_RT = []
    time.sleep(60)
    print("Reset")
    print("#######################################################")
